# generative layered cipher tools

tools for working with the vigènere cipher in a generative way based on
diceware corpuses of arbitrary size. a novel result is a generative  

multimodal alphabetic [ciphers].

try:

```
$ npm run gen/input 'colorless green ideas sleep furiously' --silent
{
  "$id": "101116379366161499951",
  "passphrase": [
    "towery",
    "prosier",
    "paintiest",
    "deforest",
    "bind",
    "ramping",
    "fading",
    "nonregimented"
  ],
  "legend": "towerypsiandfbmg",
  "key": [
    "ndesaeppmaaayo",
    "ydpripgdrymsddw",
    "ewbmybnenionpr",
    "debsirfmneanron"
  ],
  "text": "colorless green ideas sleep furiously",
  "encrypted": "afwibwabyiibirrndwffbyyiffdtpnsgnwbye",
  "decrypted": "gogorgessggreengideasgsgeepgfgriogsgy"
}
```

or:

```
$ alias gen.random="pseudorandomnumbergenerator" # accepts a "ceiling"
$ npm run gen/input "$(echo -n $(gen.random 1000 | tail -n 1) | openssl dgst -sha256)"
{
  "$id": "201220142141175834441369201925",
  "passphrase": [
    "equivalently",
    "consecrative",
    "randomization",
    "lightfaced",
    "conchy",
    "mooching",
    "reallocation",
    "improver"
  ],
  "legend": "equivalntycosrdmzghfp",
  "key": [
    "pspdvqgativvvrlypfua",
    "yqqzqmuzdctrfamavntp",
    "ashrfhopgloiicslyvc",
    "dupfdlyclytqpspgopu"
  ],
  "text": "1d2028ddcd746a7ee87dd0739d7435602b77d4908f96e27ebdad57b09aa27b69",
  "encrypted": "fafriecfhgiiihayphqfrofrimzvnuiiisatrhqvfcfrvezangyhisatfinvfofr",
  "decrypted": "pdppppddcdpppapeeppddppppdppppppppppdppppfppeppepdadpppppaappppp"
}
```

or: 

```
$ npm run gen/file './the-onion.html' --silent
> [redacted]
```

[0]: https://github.com/OpenGenus/cosmos/blob/master/code/cryptography/src/vigenere_cipher/vigenere_cipher.js
[cipher]: https://en.wikipedia.org/wiki/Polyalphabetic_cipher
