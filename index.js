/**
 * @filepath ./index.js
 * @fileoverview a library for generating polyalphabetic ciphers.
 * @author aha <patagnome@protonmail.com>
 * @license wtfpl, version 2
 * @since 0.0.0-novel.0
 * @version 0.0.0-novel.0
 * @kind library
 * @todo none
 * @name layeredCipher
 * @function
 * @description try:
 * $ npm run gen/input 'colorless green ideas sleep furiously' --silent
 * @todo browse _
 * @returns {Object} .
 */
'use strict'

const Random = require('random-js')
const niceware = require('niceware')
const _ = require('lodash')
const fs = require('fs')

const REP = null
const SPACE = 2 

const time = console.time
const end = console.timeEnd
const log = console.log
const review = console.table
const args = process.argv

/**
 * BespokeError.
 */
class BespokeError {
  constructor (content) {
    return new Error(content || { status: 500 })
  }
}

/**
 * If.
 *
 * @param {} condition
 */
const If = (condition) => {
  return new Promise((resolve, reject) => {
    !JSON.stringify(condition) && reject(new BespokeError())
    condition && resolve(condition)
  })
}

/**
 * once.
 *
 * @param {} fn
 */
const once = fn => {
  let called = false
  return function (...args) {
    if (called) return
    called = true
    return fn.apply(this, args)
  }
}

const inputText = args[2].indexOf('--input') !== -1
  ? args[2]
  : ''
const data = inputText !== '' ? args[3] : ''

const inputFile = args[2].indexOf('--file') !== -1
  ? args[2]
  : ''
const dataFile = inputFile !== '' ? args[3] : ''

/**
 * Vigenere.
 */
class Vigenere {
  /**
   * alphabetLen_.
   */
  static alphabetLen_ () {
    return Vigenere.ALPHABET.length
  }

  /**
   * encrypt.
   *
   * @param {} text
   * @param {} key
   */
  static encrypt (text, key) {
    let result = ''
    const preparedText = text && text
      .toString()
      .replace(new RegExp(/\\s/, 'g'), '')
      .toLowerCase()
    const preparedKey = key.toLowerCase()
    const alphabetLen = Vigenere.alphabetLen_()

    if (!preparedText) {
      log('no text provided')
      return
    } else {
      const START = 0

      for (let index = START; index < preparedText.length; index++) {
        const CHAR = preparedText[index]
        const SIZE = preparedKey.length
        const ID = index % SIZE
        const keyChar = preparedKey[ID]
        const shift = Vigenere.ALPHABET.indexOf(keyChar) % alphabetLen
        const indexInAlphabet = Vigenere.ALPHABET.indexOf(CHAR)

        result += Vigenere.ALPHABET[(indexInAlphabet + shift) % alphabetLen]
      }
    }

    return result
  }

  /**
   * decrypt.
   *
   * @param {} encrypted
   * @param {} key
   */
  static decrypt (encrypted, key) {
    let result = ''
    const preparedKey = key.toLowerCase()
    const alphabetLen = Vigenere.alphabetLen_()

    if (!encrypted) {
      return new BespokeError({ status: 'no text provided' })
    }

    for (let index = 0; index < encrypted.length; index++) {
      const CHAR = encrypted[index]
      const keyChar = preparedKey[index % preparedKey.length]

      let shift = (
        Vigenere.ALPHABET.indexOf(CHAR) - Vigenere.ALPHABET.indexOf(keyChar)
      )

      shift = (shift >= 0)
        ? shift
        : (alphabetLen + shift)

      result += Vigenere.ALPHABET[shift]
    }

    return result
  }
}

/**
 * gen.
 *
 * @param {} arr
 * @param {} limit
 */
function gen (DATA, limit) {
  /**
   * @constant
   * @default
   */
  const START = 0

  // constants above will not be visible in Tagbar 👉 (see "gen")
  let temp = null
  let ID = null
  var IDS = []
  var list = []

  for (let index = START; index < limit; ++index) {
    ID = random.integer(index, DATA.length)
    // @todo use cuid instead for horizontally-scalable guids
    temp = DATA[ID]
    list.push(temp)
    IDS.push(ID + '')
  }

  return {
    k: list.join(''),
    j: IDS.join('')
  }
}

// Vigenere.ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
// Vigenere.ALPHABET = 'the cat is on the mat'

const lengthOfPassphrase = (process.argv[2] || [])
const LENGTHINPUT = '--length='
const SIZE = 16
const filteredList = lengthOfPassphrase.includes(LENGTHINPUT)
const size = filteredList ? lengthOfPassphrase : [LENGTHINPUT, SIZE].join('')
const ids = size && size.split(LENGTHINPUT)
const IDS = ids[1]
const IDENT = _.isNaN(IDS)
const defaultSize = !IDENT ? IDS : SIZE
const _defaultSize = parseInt(defaultSize, 10)
const passphrase = niceware.generatePassphrase(_defaultSize)

// @description distributed interpreter.
// @note implement 🐏, 👨, etc.
Vigenere.ALPHABET = passphrase
  .join('')
  .split('')
  .filter((item, index, list) => {
    return list.indexOf(item) === index
  }).join('')

const legend = Vigenere.ALPHABET
const Rand = Random.engines
const arr = Rand.mt19937().seedWithArray([0, 0, 0, 0])
const rt = new Random(arr)
const num = rt.integer(0, 1000)
log(num)

const random = new Random(Random.engines.mt19937().autoSeed())

/**
 * @description naming pseudo-monopresence in keyware (diceware, fwp, ring signature graphs, dpki, etc.)
 */
// why generate possible redundancy?
const presence1 = gen(legend, legend.length)
const presence2 = gen(legend, legend.length)
const presence3 = gen(legend, legend.length)
const presence4 = gen(legend, legend.length)

/**
 * readDataFile.
 *
 * @param {} dataFile
 */
const readDataFile = async (dataFile) => {
  const FILEPATH = dataFile
  const DATA = fs.readFileSync(FILEPATH)
  const encrypted = await Vigenere.encrypt(DATA, presence1.k)
  const decrypted = Vigenere.decrypt(encrypted, presence1.k)

  var patient = _.extend({}, {
    $id: presence1.j,
    passphrase: passphrase,
    legend: legend,
    key: [
      presence1.k,
      presence2.k,
      presence3.k,
      presence4.k
    ],
    filepath: FILEPATH,
    encrypted: encrypted,
    decrypted: decrypted
  })

  const diagnostic = JSON.stringify(patient, REP, SPACE)

  review(diagnostic)
}

/**
 * readText.
 */
const readText = async () => {
  const text = data
  const encrypted = Vigenere.encrypt(text, presence1.k)
  const decrypted = Vigenere.decrypt(encrypted, presence1.k)

  const patient = _.extend({}, {
    $id: presence1.j,
    passphrase: passphrase,
    legend: legend,
    key: [
      presence1.k,
      presence2.k,
      presence3.k,
      presence4.k
    ],
    text: text,
    encrypted: encrypted,
    decrypted: decrypted
  })

  const diagnostic = JSON.stringify(patient, REP, SPACE)

  review(diagnostic)
}

if (dataFile !== '') {
  time('fired:once')
  once(readDataFile(dataFile))
  end('fired:once')
} else {
  time('fired:once')
  readText()
  end('fired:once')
}

module.exports = {
  readDataFile: readDataFile,
  readText: readText
}

// EOF
