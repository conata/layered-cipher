/**
 * @filepath ./mast.js
 * @description # mastodon tools
 */

// ## dependencies

require('dotenv').config()
const Mastodon = require('mastodon-api')

/**
 * @constant
 * @default
 */
const DEBUG = process.argv[2].includes('--debug') || false
const key = process.env.key || null
const debugContent = process.argv[2] || 'DEBUG=on'
const content = process.argv[3] || '?'
const VERSION = '1'
const BASE_URL = `https://botsin.space/api/v${VERSION}/`

// ## tools

/**
 * @name log
 * @param {} DATA
 */
const log = (DATA) => console.log(DATA)
const table = (DATA) => console.table(DATA)

/**
 * @name review
 * @param {} DATA
 */
const review = (DATA) => {
  return console.log(DATA)
}

/**
 * @name initialize_mastodon
 */
const initializeMastodon = () => {
  const M = new Mastodon({
    access_token: key,
    api_url: BASE_URL
  })

  return M
}

const botsinspace = initializeMastodon()

/**
 * create.
 * @param {Object} payload .
 */
const create = (payload) => {
  const postContent = payload || { status: 'noop' }

  botsinspace
    .post('statuses', postContent)
    .then((response) => {
      table(response.data.url)
      return response
    })
    .catch(review)
}

/**
 * @name read
 * @description read a bespoke list or line.
 * @param {String} type .
 */
const read = (type) => {
  botsinspace
    .get(type)
    .then(response => {
      const DATA = response && response.data
      log(DATA)
    })
    .catch(review)
}

const data = {
  status: content
}

// DEBUG && review(data)
// DEBUG && read('timelines/home')
// DEBUG && read('lists')
DEBUG && create(data)

module.exports = {
  create: create,
  read: read
}

// EOF
