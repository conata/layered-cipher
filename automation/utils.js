/**
 * @description # utils
 */

/**
 * randomIntegerInRange.
 *
 * @param {} min
 * @param {} max
 */
const randomIntegerInRange = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

/**
 * noop.
 */
const noop = () => ({})

module.exports = {
  randomIntegerInRange,
  noop
}

// EOF
