/**
 * @filepath ./automation/launch.js
 * @description # launch
 */

// ## includes

const playwright = require('playwright')

// ## constants

/**
 * @constant
 * @default
 */
const URL_ACTION = 'https://example.com'

/**
 * @constant
 * @default
 */
const CONTEXT = {
  path: 'launch-example.com.png'
}

/**
 * init.
 */
const init = async () => {
  const browser = await playwright.chromium.launch()
  const page = await browser.newPage()

  await page.goto(URL_ACTION)
  await page.screenshot(CONTEXT)

  await browser.close()
}

init()

// EOF
