/**
 * @filepath ./automation/interact.js
 * @description # interact
 */

// ## includes

const playwright = require('playwright')

/**
 * @constant
 * @default
 */
const URL_ACTION = 'https://example.com'

/**
 * @constant
 * @default
 */
const MODAL_BUTTON_SELECTOR = '.modal-footer > button'

/**
 * @constant
 * @default
 */
const CONTEXT = {
  path: 'interact-example.com.png'
}

/**
 * init.
 */
const init = async () => {
  const browser = await playwright.chromium.launch()
  const page = await browser.newPage()

  await page.goto(URL_ACTION)
  await page.waitFor(200)
  await page.click(MODAL_BUTTON_SELECTOR)
  await page.screenshot(CONTEXT)

  await browser.close()
}

init()

// EOF
