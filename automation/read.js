/**
 * @filepath ./automation/read.js
 * @description # read
 */

// ## includes

const playwright = require('playwright')
const { randomIntegerInRange } = require('./utils')

// ## tools
// ## constants

/**
 * @constant
 * @default
 */
const DATA = process.argv && process.argv[2]

/**
 * @constant
 * @default
 */
const MODAL_BUTTON_SELECTOR = '.modal-footer > button'

/**
 * @constant
 * @default
 */
const SEARCH_SELECTOR = 'input[placeholder=Search]'

/**
 * @constant
 * @default
 */
const LOCATION_SELECTOR = 'li.active > a'

/**
 * @constant
 * @default
 */
const RESULTS_SELECTOR = '.results-tab'

/**
 * @constant
 * @default
 */
const KEYBOARD_INPUT = DATA && process.argv[3]

/**
 * @constant
 * @default
 */
const URL_ACTION = DATA || 'https://example.com'

/**
 * @constant
 * @default
 */
const NAME = [
  'read',
  '-',
  String(randomIntegerInRange(0, 1000)),
  '.png'
].join('')

/**
 * @constant
 * @default
 */
const CONTEXT = {
  path: NAME
}

// ## inputs
// ## program

/**
 * init.
 */
const init = async () => {
  const browser = await playwright.chromium.launch()
  const page = await browser.newPage()

  await page.goto(URL_ACTION)
  await page.waitFor(200)

  // await page.click(MODAL_BUTTON_SELECTOR)
  // await page.waitFor(300)

  // await page.click(SEARCH_SELECTOR)
  // await page.keyboard.type(KEYBOARD_INPUT)
  // await page.waitForSelector(LOCATION_SELECTOR)

  // await page.click(LOCATION_SELECTOR)
  // await page.waitForSelector(`${RESULTS_SELECTOR} > p`)

  // const results = await page.$(RESULTS_SELECTOR)
  // const text = await results.evaluate(element => element.innerText)
  // console.log(text)

  await page.screenshot(CONTEXT)

  await browser.close()
}

// ## output
// ## outcome (e.g., export default above)
// ## try

init()

// ## test

// EOF
