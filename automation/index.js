/**
 * @filepath ./automation/index.js
 * @description .
 */
const playwright = require('playwright')
const { randomIntegerInRange } = require('./utils')

const URL_ACTION = 'https://example.com'
const WHATEVER_URL = 'https://example.com'
const INPUT = (process.argv && process.argv[2])
const FLAG = INPUT && process.argv.filter((i) => {
  const C = '--validate='
  return i.includes(C)
})[0]
const someWebUrl = INPUT || WHATEVER_URL

/**
 * @constant
 * @default
 */
const NAME = [
  'read',
  '-',
  String(randomIntegerInRange(0, 1000)),
  '.png'
].join('')

/**
 * @constant
 * @default
 */
const CONTEXT = {
  path: NAME
}

/**
 * run.
 */
const run = async (url) => {
  const URL_GIVEN = url || URL_ACTION
  const browser = await playwright.chromium.launch()
  const page = await browser.newPage()

  const cacheBust = [
    String(randomIntegerInRange(0, 1000)),
    String(randomIntegerInRange(0, 1000)),
    String(randomIntegerInRange(0, 1000)),
    String(randomIntegerInRange(0, 1000))
  ].join('')

  const D = INPUT && process.argv[3]

  const ACTING_URL = [
    URL_GIVEN,
    '?',
    'cache_bust=',
    (D || cacheBust)
  ].join('')

  const VALIDATING_URL = [
    'https://validator.w3.org/nu/?showsource=yes&showoutline=yes&showimagereport=yes&doc=',
    URL_GIVEN,
    '&',
    'cache_bust=',
    (D || cacheBust)
  ].join('')

  !FLAG && console.log('visiting:', ACTING_URL)
  FLAG && console.log('validating:', VALIDATING_URL)

  await page.goto(FLAG ? VALIDATING_URL : ACTING_URL)
  await page.waitFor(200)

  await page.screenshot(CONTEXT)

  await browser.close()
}

run(someWebUrl)

// EOF
