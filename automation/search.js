/**
 * @filepath ./automation/search.js
 * @description # search
 * the following scenario involves an [eventual interactivity] with a form
 * within a modal window. we might call this a paradigmatic workflow.
 */

// ## includes

const playwright = require('playwright')
const { randomIntegerInRange } = require('./utils')

// ## tools
// ## constants

/**
 * @constant
 * @default
 * @description found a typical button that invokes a modal window. is this
 * modal already open?
 */
const MODAL_BUTTON_SELECTOR = '.modal-footer > button'

/**
 * @constant
 * @default
 * @description found a search input.
 */
const SEARCH_SELECTOR = 'input[placeholder=Search]'

/**
 * @constant
 * @default
 */
const LOCATION_SELECTOR = 'li.active > a'

/**
 * @constant
 * @default
 */
const RAND = String(randomIntegerInRange(0, 1000))

/**
 * @constant
 * @default
 */
const NAME = [
  'search-example.com',
  '-',
  RAND,
  '.png'
].join('')

/**
 * @constant
 * @default
 */
const CONTEXT = {
  path: NAME
}

const DEFAULT_URL_ACTION = 'https://example.com'

/**
 * @constant
 * @default
 */
const URL_ACTION = process.argv[2] || DEFAULT_URL_ACTION

/**
 * @constant
 * @default
 */
const KEYBOARD_INPUT = process.argv[2] && process.argv[3]

/**
 * init.
 */
const init = async () => {
  const browser = await playwright.chromium.launch()
  const page = await browser.newPage()

  await page.goto(URL_ACTION)
  await page.waitFor(200)
  await page.click(MODAL_BUTTON_SELECTOR)
  await page.waitFor(300)

  await page.click(SEARCH_SELECTOR)
  await page.keyboard.type(KEYBOARD_INPUT)
  await page.waitForSelector(LOCATION_SELECTOR)

  await page.screenshot(CONTEXT)

  await browser.close()
}

init()

// EOF
