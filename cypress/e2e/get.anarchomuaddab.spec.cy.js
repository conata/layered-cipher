/**
 * @description nothing fancy here it just hits a page and runs a preflight 
 * request for some reason. i don't care if i don't know javascript.
 */
const rand = (rangeOfDays, startHour, hourRange) => {
  const today = new Date(Date.now())
  return new Date(today.getYear() + 1900, today.getMonth(),
    today.getDate() + Math.random() * rangeOfDays,
    Math.random() * hourRange + startHour,
    Math.random() * 60)
}
const TIMEOUT = 12000
const DATESEED = rand(2, 8, 2)
const INSECURE_RANDOM_NUMBER = Math.ceil(Math.random() * 100000)
const DATA = INSECURE_RANDOM_NUMBER.toString() + '-a46e37632fa6ca51a13fe39a567b3c23b28c2f47d8af6be9bd63e030e214ba38'
const PROFILE_URL = 'anarchomuaddab'
var ENTRYPOINT_URL = [
  'https://twitter.com/',
  PROFILE_URL,
  '?entrypoint=' + DATA,
  '&',
  '_=' + DATA
].join('')

describe('GET ' + PROFILE_URL, () => {
  it('preflight request then should visit', () => {
    cy.log('-- DATESEED: ' + DATESEED + '--------')
    cy.log('-- DATA_ID: ' + DATA + '--------')
    cy.log('-- calling -------- ' + ENTRYPOINT_URL)
    cy.request(ENTRYPOINT_URL).then((doc) => {
      if (doc && doc.body) {
        cy.wait(4).then(() => {
          cy.visit(ENTRYPOINT_URL).then((contentWindow) => {
            cy.wait(TIMEOUT).then(() => {
              const C = contentWindow?.document.documentElement
              if (C && C.innerHTML) {
                const nameOf = [
                  'cypress/fixtures/',
                  PROFILE_URL,
                  '-output.html'
                ].join('')

                cy.writeFile(nameOf, C.innerHTML)
              }
              contentWindow.document.close()
            })
          })
        })
      }
    })
  })
})

// EOF
